﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Northwind.API.Models;
using Northwind.BLL;
using Northwind.Core.Abstractions;
using Northwind.Core.Abstractions.Repositories;
using System.Linq;
using System.Threading.Tasks;

namespace NorthwindAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrdersController:ControllerBase
    {
        IOrdersBL _orders;

        public OrdersController(IOrdersBL orders)
        {
            _orders = orders;
        }

        [HttpGet("avg_freight")]//25
        public async Task<IActionResult> AvgFreightAsync()
        {
            var response =  _orders.AvgFreight();

            return (response!=null)? Ok(response):NoContent();
        }

        [HttpGet("avg_freight_1998")]//26
      public async Task<IActionResult> AvgFreight98Async()
        {
            var response = _orders.AvgFreight98();

            return (response != null) ? Ok(response) : NoContent();
        }


    }
}
