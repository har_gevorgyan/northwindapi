﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Northwind.API.Models;
using Northwind.Core.Abstractions.Repositories;
using System.Linq;
using System.Threading.Tasks;

namespace Northwind.API.Controllers
{ 
    public class CustomersController : ControllerBase
    {
        ICustomersBL _customers;
        public CustomersController(ICustomersBL customers)
        {
            _customers = customers;
        }
        [HttpGet("total_customers")]//21
        public async Task<IActionResult> TotalCustomersAsync()
        {
            var response = _customers.TotalCustomers();

            return (response!=null) ? Ok(response) : NoContent();
        }
        [HttpGet("order")]//24
        public async Task<IActionResult> OrderByRegionAsync()
        {
            var response = _customers.OrderByRegion();

            return (response!= null) ? Ok(response) : NoContent();
        }
    }
}
