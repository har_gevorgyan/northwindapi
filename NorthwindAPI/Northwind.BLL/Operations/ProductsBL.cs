﻿using Northwind.Core.Abstractions.Repositories;
using Northwind.DAL.BigOperations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Northwind.BLL.Operations
{

    public class ProductsBL : IProductsBL
    {
        BigProducts products;
        public Task NeedReordering()
        {
            return products.NeedReordering();
        }

        public Task NeedReorderingContinued()
        {
            return products.NeedReorderingContinued();
        }
    }
}
