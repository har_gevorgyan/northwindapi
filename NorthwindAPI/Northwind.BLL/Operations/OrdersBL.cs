﻿
using Northwind.Core.Abstractions.Repositories;
using Northwind.DAL.BigOperations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Northwind.BLL.Operations
{
    public class OrdersBL : IOrdersBL
    {
        BigOrders orders;
        public  Task AvgFreight()
        {
             return  orders.AvgFreight();
        }

        public Task AvgFreight98()
        {
            return orders.AvgFreight98();
        }
    }
}
