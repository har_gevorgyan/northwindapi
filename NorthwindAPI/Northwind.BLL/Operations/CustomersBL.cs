﻿using Northwind.Core.Abstractions.Repositories;
using Northwind.DAL.BigOperations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Northwind.BLL.Operations
{
    public class CustomersBL : ICustomersBL
    {
        BigCustomers customers;
        public Task OrderByRegion()
        {
            return customers.OrderByRegion();
        }

        public Task TotalCustomers()
        {
            return customers.TotalCustomers();
        }
    }
}
