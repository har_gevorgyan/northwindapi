﻿using Microsoft.EntityFrameworkCore;
using Northwind.Core.Abstractions;
using Northwind.Core.Abstractions.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Northwind.DAL.BigOperations
{
    public class BigOrders : IOrdersBL
    {
        protected readonly NORTHWNDContext _db;

        public Task AvgFreight()
        {
            var response = _db.Orders.Select(p => new { p.ShipCountry, p.Freight }).
                GroupBy(g => g.ShipCountry).Select(g => new
                {
                    ShipCountry = g.Key,
                    AvgFreight = g.Average(g => g.Freight)
                }).Take(3).OrderBy(x => x.AvgFreight).ToListAsync();

            return (response.Result.Count != 0) ? response : null;
        }

        public Task AvgFreight98()
        {
            var response = _db.Orders.Select(p => new { p.ShipCountry, p.Freight, p.OrderDate }).Where(d => d.OrderDate.Value.Year == 1998).
                GroupBy(g => g.ShipCountry).Select(g => new
                {
                    ShipCountry = g.Key,
                    AvgFreight = g.Average(g => g.Freight)
                }).Take(3).OrderBy(x => x.AvgFreight).ToListAsync();

            return (response.Result.Count!=0)?response:null;
        }
    }
}
