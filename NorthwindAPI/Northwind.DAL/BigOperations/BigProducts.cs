﻿using Microsoft.EntityFrameworkCore;
using Northwind.Core.Abstractions.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Northwind.DAL.BigOperations
{
    public class BigProducts : IProductsBL
    {
        NORTHWNDContext _db;
        public Task NeedReordering()
        {
            var response =  _db.Products.Select(p => new
            {
                p.ProductId,
                p.ProductName,
                p.UnitsOnOrder,
                p.ReorderLevel
            }).Where(p => p.UnitsOnOrder < p.ReorderLevel).OrderBy(p => p.ProductId).ToListAsync();

            return (response.Result.Count > 0)?response:null;
        }

        public Task NeedReorderingContinued()
        {
            var response =  _db.Products.Select(p => new
            {
                p.ProductId,
                p.UnitsInStock,
                p.UnitsOnOrder,
                p.ReorderLevel,
                p.Discontinued
            }).
                            Where(p => (p.UnitsOnOrder + p.UnitsInStock) <= p.ReorderLevel && p.Discontinued == false)
                            .OrderBy(p => p.ProductId).ToListAsync();

            return (response.Result.Count > 0)?response:null;
        }
    }
}
