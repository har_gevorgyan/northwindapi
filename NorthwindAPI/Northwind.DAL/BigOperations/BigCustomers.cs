﻿using Microsoft.EntityFrameworkCore;
using Northwind.Core.Abstractions.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Northwind.DAL.BigOperations
{
    public class BigCustomers : ICustomersBL
    {
        protected readonly NORTHWNDContext _db;
        public Task OrderByRegion()
        {
            var response =  _db.Customers.Select(c => new
            {
                c.CustomerId,
                c.CompanyName,
                c.Region
            }).OrderBy(o => o.Region == null ? 1 : 0).ToListAsync();

            return (response.Result.Count != 0) ? response : null;

        }

        public Task TotalCustomers()
        {
            var response =  _db.Customers.Select(x => new
            {
                x.Country,
                x.City,
                x.CustomerId
            }).GroupBy(x => new { x.Country, x.City }).Select(x => new
            {
                Country = x.Key.Country,
                City = x.Key.City,
                Customers = x.Select(x => x.CustomerId).Count()
            }).OrderByDescending(x => x.Customers).ToListAsync();

            return (response.Result.Count != 0) ? response : null;
        }
    }
}
