﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Northwind.Core.Abstractions.Repositories
{
    public interface IOrdersBL
    {
        Task AvgFreight();
        Task AvgFreight98();

    }
}
