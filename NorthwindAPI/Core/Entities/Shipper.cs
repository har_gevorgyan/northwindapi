﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Northwind.Core.Models
{
    public partial class Shipper
    {
        public Shipper()
        {
            Orders = new HashSet<T>();
        }

        public int ShipperId { get; set; }
        public string CompanyName { get; set; }
        public string Phone { get; set; }

        public virtual ICollection<T> Orders { get; set; }
    }
}
